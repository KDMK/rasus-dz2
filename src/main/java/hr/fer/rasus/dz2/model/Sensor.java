package hr.fer.rasus.dz2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class Sensor {

    private String username;
    private Double longitude;
    private Double latitude;
    private String IPAddress;
    private Reading currentReading;
    private int port;

    private Map<String, Float> measurements;
    private Sensor nearestNeighbour;
}
