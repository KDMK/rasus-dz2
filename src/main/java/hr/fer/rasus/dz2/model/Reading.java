package hr.fer.rasus.dz2.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Reading {

    private Double temperature;
    private Double pressure;
    private Double humidity;
    private Double co;
    private Double no2;
    private Double so2;
}
