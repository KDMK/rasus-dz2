/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */
package hr.fer.rasus.dz2.server;

import hr.fer.rasus.dz2.network.SimpleSimulatedDatagramSocket;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class StupidUDPServer implements Runnable {

    private static final Logger log = getLogger(StupidUDPServer.class);

    private final int PORT; // server port

    public StupidUDPServer(int port) {
        this.PORT = port;
    }

    @Override
    public void run() {
        byte[] rcvBuf = new byte[256]; // received bytes
        byte[] sendBuf = new byte[256];// sent bytes
        String rcvStr;

        // create a UDP socket and bind it to the specified port on the local
        // host
        DatagramSocket socket = null; //SOCKET -> BIND
        try {
            socket = new SimpleSimulatedDatagramSocket(PORT, 0.2, 200);
        } catch (SocketException e) {
            log.error("Cannot open socket on port {}", PORT);
            return;
        }

        while (true) {
            // create a DatagramPacket for receiving packets
            DatagramPacket packet = new DatagramPacket(rcvBuf, rcvBuf.length);

            // receive packet
            try {
                socket.receive(packet);
            } catch (IOException e) {
                log.error("Error while receiving packet.");
                continue;
            }

            // construct a new String by decoding the specified subarray of
            // bytes
            // using the platform's default charset
            rcvStr = new String(packet.getData(), packet.getOffset(),
                    packet.getLength());
            log.info("Server received: " + rcvStr);

            // encode a String into a sequence of bytes using the platform's
            // default charset
            sendBuf = rcvStr.toUpperCase().getBytes();
            log.info("Server sends: " + rcvStr.toUpperCase());

            // create a DatagramPacket for sending packets
            DatagramPacket sendPacket = new DatagramPacket(sendBuf,
                    sendBuf.length, packet.getAddress(), packet.getPort());

            // send packet
            try {
                socket.send(sendPacket);
            } catch (IOException e) {
                log.error("Error while sending the packet.");
            }
        }
    }
}
