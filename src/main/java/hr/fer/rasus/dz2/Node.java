package hr.fer.rasus.dz2;

import hr.fer.rasus.dz2.client.UDPClient;
import hr.fer.rasus.dz2.clock.EmulatedSystemClock;
import hr.fer.rasus.dz2.server.StupidUDPServer;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.internal.util.PropertiesHelper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * Node class simulates single sensor node. Its intended use is with other nodes
 * as a distributed sensor array. Each node synchronizes itself with all other
 * nodes. TODO: finish this documentation
 */
public class Node {

    private static final Logger log = getLogger(Node.class);

    /**
     * Staring point of simulated node
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            log.error("Invalid number of arguments! Please provide unique node " +
                    "name and location of node properties file.");
            System.exit(127);
        }

        log.info("Node initializing");

        log.info("Reading properties from {}", args[1]);
        try {
            Properties props = loadProperties(args[1]);
            log.info("Node properties succesfully applied.");
        } catch (IOException e) {
            log.error("Couldn't load properties!");
            System.exit(255);
        }

        EmulatedSystemClock clock = new EmulatedSystemClock();

        log.info("Starting UDP server");
        Thread nodeServer = new Thread(new StupidUDPServer(1234));

        log.info("Starting UDP client");
        Thread nodeClient = new Thread(new UDPClient(1234, clock));

        log.info("Node succesfully started");

        try {
            nodeServer.join();
            nodeClient.join();
        } catch (InterruptedException e) {
            log.error("Interrupted while waiting threads to join.");
            e.printStackTrace();
        }

        log.info("Shutting down node. Goodbye!");
    }

    private static Properties loadProperties(String propFileName) throws IOException {
        Properties prop = new Properties();

        InputStream inputStream = Node.class.getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        return prop;
    }
}
