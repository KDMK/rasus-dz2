/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Eengineering and Computing, University of Zagreb.
 */
package hr.fer.rasus.dz2.client;

import hr.fer.rasus.dz2.clock.EmulatedSystemClock;
import hr.fer.rasus.dz2.model.Reading;
import hr.fer.rasus.dz2.network.SimpleSimulatedDatagramSocket;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.*;
import java.util.List;

import static hr.fer.rasus.dz2.client.ClientUtil.getReadingsFromFile;
import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class UDPClient implements Runnable {

    private static List<Reading> readings = getReadingsFromFile();

    private static final Logger log = getLogger(UDPClient.class);

    private final int PORT;
    private EmulatedSystemClock clock;

    public UDPClient(int port, EmulatedSystemClock clock) {
        this.PORT = port;
        this.clock = clock;
    }

    @Override
    public void run() {
        String sendString = "Any string...";

        byte[] rcvBuf = new byte[256]; // received bytes

        // encode this String into a sequence of bytes using the platform's
        // default charset and store it into a new byte array

        // determine the IP address of a host, given the host's name
        InetAddress address;
        try {
            address = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            log.error(e.getMessage());
            return;
        }

        // create a datagram socket and bind it to any available
        // port on the local host
        //DatagramSocket socket = new SimulatedDatagramSocket(0.2, 1, 200, 50); //SOCKET
        DatagramSocket socket; //SOCKET
        try {
            socket = new SimpleSimulatedDatagramSocket(0.2, 200);
        } catch (SocketException e) {
            log.error(e.getMessage());
            return;
        }

        log.info("Client sends: ");
        // send each character as a separate datagram packet
        for (int i = 0; i < sendString.length(); i++) {
            byte[] sendBuf = new byte[1];// sent bytes
            sendBuf[0] = (byte) sendString.charAt(i);

            // create a datagram packet for sending data
            DatagramPacket packet = new DatagramPacket(sendBuf, sendBuf.length,
                    address, PORT);

            // send a datagram packet from this socket
            try {
                socket.send(packet); //SENDTO
            } catch (IOException e) {
                log.error("Error while sending packet");
                continue;
            }
            log.info(new String(sendBuf));
        }

        StringBuilder receiveString = new StringBuilder();

        while (true) {
            // create a datagram packet for receiving data
            DatagramPacket rcvPacket = new DatagramPacket(rcvBuf, rcvBuf.length);

            try {
                // receive a datagram packet from this socket
                socket.receive(rcvPacket); //RECVFROM
            } catch (SocketTimeoutException e) {
                break;
            } catch (IOException ex) {
                log.error(ex);
            }

            // construct a new String by decoding the specified subarray of bytes
            // using the platform's default charset
            receiveString.append(new String(rcvPacket.getData(), rcvPacket.getOffset(), rcvPacket.getLength()));

        }
        log.info("Client received: " + receiveString);

        // close the datagram socket
        socket.close(); //CLOSE
    }
}
