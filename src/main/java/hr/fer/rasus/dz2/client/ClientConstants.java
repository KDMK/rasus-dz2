package hr.fer.rasus.dz2.client;

public class ClientConstants {

    static final double LONGITUDE_LOWER_LIMIT = 15.87;
    static final double LONGITUDE_UPPER_LIMIT = 16;

    static final double LATITUDE_LOWER_LIMIT = 45.75;
    static final double LATITUDE_UPPER_LIMIT = 45.85;

    static final String HEADER_TEMPERATURE = "Temperature";
    static final String HEADER_PRESSURE = "Pressure";
    static final String HEADER_HUMIDITY = "Humidity";
    static final String HEADER_CO = "CO";
    static final String HEADER_NO2 = "NO2";
    static final String HEADER_SO2 = "SO2";

    static final String[] HEADERS = {
            HEADER_TEMPERATURE,
            HEADER_PRESSURE,
            HEADER_HUMIDITY,
            HEADER_CO,
            HEADER_NO2,
            HEADER_SO2
    };

    static final String READINGS_CSV_PATH = "src/main/resources/mjerenja.csv";
}
