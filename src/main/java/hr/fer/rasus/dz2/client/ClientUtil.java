package hr.fer.rasus.dz2.client;


import hr.fer.rasus.dz2.model.Reading;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


import static hr.fer.rasus.dz2.client.ClientConstants.*;
import static org.apache.logging.log4j.LogManager.getLogger;

class ClientUtil {

    private static final Logger log = getLogger(ClientUtil.class);

    static List<Reading> getReadingsFromFile() {
        Path measurementsCsvPath = Paths.get(READINGS_CSV_PATH).toAbsolutePath();
        File measurementCsvFile = measurementsCsvPath.toFile();

        Reader in;
        Iterable<CSVRecord> records = null;
        log.info("Reading {}", READINGS_CSV_PATH);
        try {
            in = new FileReader(measurementCsvFile);

            records = CSVFormat.DEFAULT
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);
        } catch (IOException e) {
            log.error("Cannot read file {}", READINGS_CSV_PATH);
            e.printStackTrace();
        }

        assert records != null;
        List<Reading> readings = new ArrayList<>();
        records.forEach(record -> {
            Reading r = Reading.builder()
                    .temperature(getDoubleParameter(record, HEADER_TEMPERATURE))
                    .pressure(getDoubleParameter(record, HEADER_PRESSURE))
                    .humidity(getDoubleParameter(record, HEADER_HUMIDITY))
                    .co(getDoubleParameter(record, HEADER_CO))
                    .no2(getDoubleParameter(record, HEADER_NO2))
                    .so2(getDoubleParameter(record, HEADER_SO2))
                    .build();
            readings.add(r);
        });

        log.info("Succesfully initiated measurements from file {}", READINGS_CSV_PATH);
        return readings;
    }

    private static Double getDoubleParameter(CSVRecord record, String columnName) {
        String colValue = record.get(columnName);

        return colValue.trim().isEmpty() ? null : Double.parseDouble(colValue);
    }
}
